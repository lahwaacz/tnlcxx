# TNLCXX

`tnlcxx` is is a wrapper designed to simplify the building process of simple projects based on the [Template Numerical Library](https://gitlab.com/tnl-project/tnl) that require compilation of only one `.cpp` or `.cu` source file.

## Instalation

1. Clone the repository as follows:

   ```bash
   git clone https://gitlab.com/tnl-project/tnlcxx.git
   ```

2. Copy the wrapper into an appropriate `bin` directory. For example:

   ```bash
   cp tnlcxx/tnlcxx ~/.local/bin
   ```

   Ensure that the target directory is in your `$PATH` environment variable.

## Usage

Below are examples showcasing how to use `tnlcxx` in the most common situations.

1. Building an application just for CPU (with debugging information):

   ```bash
   tnlcxx main.cpp
   ```

2. Building an application just for CPU in release configuration, i.e., with optimizations and no debugging information:

   ```bash
   tnlcxx --release main.cpp
   ```

3. Building an application for both CPU and GPU (with debugging information):

   ```bash
   tnlcxx main.cu
   ```

4. Building an application for both CPU and GPU in release configuration, i.e., with optimizations and no debugging information:

   ```bash
   tnlcxx --release main.cu
   ```

   Note that `nvcc` compiler may not generate optimal code for the CPU.

5. Bulding an application with MPI support:

   ```bash
   tnlcxx --mpi main.cpp
   ```

6. Building an application with MPI support for both CPU and GPU in the release configuration:

   ```bash
   tnlcxx --mpi --release main.cu
   ```

Additional compiler options may be added as follows:

```bash
tnlcxx main.cpp -- <compiler options>
```

The wrapper is using `cmake` to configure the build process. Therefore, the first execution take a bit longer as the configuration needs to be done. However, any subsequent calls perform the build only.

## Beautiful stack trace in debug build

`tnlcxx` uses [Backward](https://github.com/bombela/backward-cpp) to print a beautiful stack trace when the compiled program aborts.
Note that Backward is obtained using CMake's [FetchContent](https://cmake.org/cmake/help/latest/module/FetchContent.html) module, which downloads Backward sources from GitHub.
Hence, the system must be online, otherwise Backward will not be used.

Consider a short demo:

```cpp
#include <TNL/Algorithms/parallelFor.h>

int main()
{
    int* data = nullptr;
    TNL::Algorithms::parallelFor<TNL::Devices::Sequential>(0, 100, [=](int i) {
            if (data == nullptr)
                throw std::runtime_error("attempted to assign via a nullptr");
            data[i] = i;
        });
}
```

When compiled without Backward, e.g. using `tnlcxx --release`, running the program results in a traditional, very terse error message:
```
terminate called after throwing an instance of 'std::runtime_error'
  what():  attempted to assign via a nullptr
Aborted (core dumped)
```

But when compiled with Backward using `tnlcxx --debug`, the program prints a beautiful stack trace including code snippets:
```
terminate called after throwing an instance of 'std::runtime_error'
  what():  attempted to assign via a nullptr
Stack trace (most recent call last):
#15   Object "", at 0xffffffffffffffff, in
#14   Object "/home/user/example", at 0x560b0b48b7a4, in _start
#13   Source "../csu/libc-start.c", line 360, in __libc_start_main_impl [0x7f5d2cc27d89]
#12   Source "../sysdeps/nptl/libc_start_call_main.h", line 58, in __libc_start_call_main [0x7f5d2cc27ccf]
#11   Source "/home/user/example.cpp", line 6, in main [0x560b0b48b950]
          3: int main()
          4: {
          5:     int* data = nullptr;
      >   6:     TNL::Algorithms::parallelFor<TNL::Devices::Sequential>(0, 100, [=](int i) {
          7:             if (data == nullptr)
          8:                 throw std::runtime_error("attempted to assign via a nullptr");
          9:             data[i] = i;
#10   Source "/usr/include/TNL/Algorithms/parallelFor.h", line 63, in parallelFor<TNL::Devices::Sequential, int, int, main()::<lambda(int)> > [0x560b0b48b9a5]
         60: parallelFor( const Begin& begin, const End& end, Function f, FunctionArgs... args )
         61: {
         62:    typename Device::LaunchConfiguration launch_config;
      >  63:    parallelFor< Device >( begin, end, launch_config, f, args... );
         64: }
         65:
         66: /**
#9    Source "/usr/include/TNL/Algorithms/parallelFor.h", line 51, in parallelFor<TNL::Devices::Sequential, int, int, main()::<lambda(int)> > [0x560b0b48b9e9]
         48:              FunctionArgs... args )
         49: {
         50:    using Index = std::common_type_t< Begin, End >;
      >  51:    detail::ParallelFor1D< Device >::exec(
         52:       static_cast< Index >( begin ), static_cast< Index >( end ), launch_config, f, args... );
         53: }
#8    Source "/usr/include/TNL/Algorithms/detail/ParallelFor1D.h", line 27, in exec<int, main()::<lambda(int)> > [0x560b0b48ba17]
         24:    exec( Index begin, Index end, typename Device::LaunchConfiguration launch_config, Function f, FunctionArgs... args )
         25:    {
         26:       for( Index i = begin; i < end; i++ )
      >  27:          f( i, args... );
         28:    }
         29: };
#7    Source "/home/user/example.cpp", line 8, in operator() [0x560b0b48b8d2]
          5:     int* data = nullptr;
          6:     TNL::Algorithms::parallelFor<TNL::Devices::Sequential>(0, 100, [=](int i) {
          7:             if (data == nullptr)
      >   8:                 throw std::runtime_error("attempted to assign via a nullptr");
          9:             data[i] = i;
         10:         });
         11: }
#6    Source "/usr/src/debug/gcc/gcc/libstdc++-v3/libsupc++/eh_throw.cc", line 98, in __cxa_throw [0x7f5d2d0b03ec]
#5    Source "/usr/src/debug/gcc/gcc/libstdc++-v3/libsupc++/eh_terminate.cc", line 58, in terminate [0x7f5d2d0b0188]
#4    Source "/usr/src/debug/gcc/gcc/libstdc++-v3/libsupc++/eh_terminate.cc", line 48, in __builtin_abort [0x7f5d2d0b011b]
#3    Source "/usr/src/debug/gcc/gcc/libstdc++-v3/libsupc++/vterminate.cc", line 95, in __verbose_terminate_handler [0x7f5d2d09ca6e]
#2    Source "/usr/src/debug/glibc/glibc/stdlib/abort.c", line 79, in abort [0x7f5d2cc264b7]
#1    Source "../sysdeps/posix/raise.c", line 26, in raise [0x7f5d2cc3e667]
#0    Source "/usr/src/debug/glibc/glibc/nptl/pthread_kill.c", line 44, in __pthread_kill_implementation [0x7f5d2cc8e83c]
Aborted (Signal sent by tkill() 35422 1000)
Aborted (core dumped)
```

In practice, it looks even more beautiful, because the main lines in the code snippets are highlighted with yellow color.
